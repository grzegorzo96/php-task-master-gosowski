<?php

namespace Recruitment\Entity;

use Recruitment\Cart\Exception\QuantityTooLowException;
use Recruitment\Cart\Item;

/**
 * Cart
 *
 * @author Grzegorz Osowski
 */
class Cart
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Item[]
     */
    private $items = [];

    /**
     * @var int
     */
    private $totalPrice;


    public function __construct()
    {
        $this->totalPrice = 0;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Cart
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Item[] $items
     * @return Cart
     */
    public function setItems(array $items): self
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        return $this->totalPrice;
    }

    /**
     * @param int $totalPrice
     * @return Cart
     */
    public function setTotalPrice(int $totalPrice): self
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    /**
     * @param Product $product
     * @param int|null $quantity
     * @return Cart
     * @throws QuantityTooLowException
     */
    public function addProduct(Product $product, int $quantity = null): Cart
    {
        if ($quantity == null) {
            $quantity = 1;
        }

        foreach ($this->items as $item) {
            if ($item->getProduct() === $product) {
                $item->setQuantity($quantity);
                $this->totalPrice += $item->getTotalPrice();
                return $this;
            }
        }

        $item = new Item($product, $quantity);
        $item->setId($product->getId());

        $this->totalPrice += $item->getTotalPrice();

        $this->items[] = $item;
        return $this;
    }

    /**
     * @param Product $product
     * @return Cart
     */
    public function removeProduct(Product $product): Cart
    {
        foreach ($this->items as $idx => $item) {
            if ($item->getProduct() === $product) {
                $this->totalPrice -= $item->getTotalPrice();
                array_splice($this->items, $idx, 1);
            }
        }

        return $this;
    }

    /**
     * @param int $idx
     * @return Item
     */
    public function getItem(int $idx): Item
    {
        if (($idx > (count($this->items) - 1)) || ($idx < 0)) {
            throw new \OutOfBoundsException("Invalid index in an array");
        }

        return $this->items[$idx];
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @return Cart
     * @throws QuantityTooLowException
     */
    public function setQuantity(Product $product, int $quantity): Cart
    {
        $this->totalPrice = 0;

        if (count($this->items) == 0) {
            $this->addProduct($product, $quantity);
        } else {
            foreach ($this->items as $item) {
                if ($item->getProduct() === $product) {
                    $item->setQuantity($quantity);
                }
                $this->totalPrice += $item->getTotalPrice();
            }
        }

        return $this;
    }

    /**
     * @param int $id
     * @return Order
     */
    public function checkout(int $id): Order
    {
        $this->id = $id;

        $order = new Order();

        $items = [];

        foreach ($this->items as $item) {
            $items[] = [
                'id' => $item->getId(),
                'quantity' => $item->getQuantity(),
                'total_price' => $item->getTotalPrice(),
                'tax' => $item->getProduct()->getTax(),
                'total_price_gross' => $item->getTotalPrice() * $this->getTaxToCalculate($item->getProduct()->getTax())
            ];
        }

        $data = [
            'id' => $this->getId(),
            'items' => $items,
            'total_price' => $this->getTotalPrice(),
            'total_price_gross' => $this->getTotalPriceGross()
        ];

        $order->setDataForView($data);

        $this->items = [];
        $this->totalPrice = 0;

        return $order;
    }

    /**
     * @return int
     */
    public function getTotalPriceGross(): int
    {
        $totalPriceGross = 0;

        foreach ($this->items as $item) {
            $tax = $this->getTaxToCalculate($item->getProduct()->getTax());

            $totalPriceGross += $item->getTotalPrice() * $tax;
        }

        return $totalPriceGross;
    }

    /**
     * @param int $tax
     * @return float
     */
    public function getTaxToCalculate(int $tax): float
    {
        switch ($tax) {
            case 0:
            default:
                return 1;
            case 5:
                return 1.05;
            case 8:
                return 1.08;
            case 23:
                return 1.23;
        }
    }
}
