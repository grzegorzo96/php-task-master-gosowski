<?php

namespace Recruitment\Entity\Exception;

/**
 * InvalidUnitPriceException
 *
 * @author Grzegorz Osowski
 */
class InvalidUnitPriceException extends \Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
