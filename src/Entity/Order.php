<?php

namespace Recruitment\Entity;

/**
 * Order
 *
 * @author Grzegorz Osowski
 */
class Order
{
    /**
     * @var array
     */
    public $dataForView;

    /**
     * @return array
     */
    public function getDataForView(): array
    {
        return $this->dataForView;
    }

    /**
     * @param array $dataForView
     */
    public function setDataForView(array $dataForView): void
    {
        $this->dataForView = $dataForView;
    }
}
