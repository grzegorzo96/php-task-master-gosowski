<?php

namespace Recruitment\Cart\Exception;

/**
 * QuantityTooLowException
 *
 * @author Grzegorz Osowski
 */
class QuantityTooLowException extends \Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
