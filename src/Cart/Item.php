<?php

namespace Recruitment\Cart;

use Recruitment\Cart\Exception\QuantityTooLowException;
use Recruitment\Entity\Product;

/**
 * Item
 *
 * @author Grzegorz Osowski
 */
class Item
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var int
     */
    private $totalPrice;

    /**
     * @param Product $product
     * @param int $quantity
     */
    public function __construct(Product $product, int $quantity)
    {
        $this->product = $product;

        if ($product->getMinimumQuantity() != null) {
            if ($product->getMinimumQuantity() > $quantity) {
                throw new \InvalidArgumentException("Quantity is less than minimum quantity of product");
            }
        }

        $this->quantity = $quantity;
        $this->totalPrice = ($product->getUnitPrice() * $quantity);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Item
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return Item
     */
    public function setProduct(Product $product): self
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Item
     * @throws QuantityTooLowException
     */
    public function setQuantity(int $quantity): self
    {
        if ($this->product->getMinimumQuantity() != null) {
            if ($this->product->getMinimumQuantity() > $quantity) {
                throw new QuantityTooLowException("Quantity is less than minimum quantity of product");
            }
        }

        $this->quantity = $quantity;

        $this->totalPrice = ($this->getTotalPrice() * $quantity);

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        return $this->totalPrice;
    }

    /**
     * @param int $totalPrice
     * @return Item
     */
    public function setTotalPrice(int $totalPrice): self
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }
}
